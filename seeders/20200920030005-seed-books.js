const fs = require('fs');
'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */

        const parseData = JSON.parse(fs.readFileSync('./books.json'));
        const booksData = [];
        parseData.forEach(data => {
            const {
                title,
                author,
                released_date,
                pages,
                genre,
            } = data;
            booksData.push({
                title,
                author,
                released_date,
                pages,
                genre,
                createdAt: new Date(),
                updatedAt: new Date()
            })
        })
        await queryInterface.bulkInsert('books', booksData, {});
    },

    down: async(queryInterface, Sequelize) => {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete('books', null, {});
    }
};