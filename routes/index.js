const { Router } = require('express');
const router = Router();
const BookRoutes = require('./book')
const BookController = require('../controller/BookController')

router.get('/', BookController.getBook)

router.use('/books', BookRoutes)
module.exports = router;