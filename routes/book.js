const { Router } = require('express');
const router = Router();
const BookController = require('../controller/BookController')

router.get('/add', BookController.addFormBook)
router.post('/add', BookController.addBook)
router.get('/delete/:id', BookController.deleteBook)
router.get('/edit/:id', BookController.updateBook)
router.get('/:id', BookController.findById)

module.exports = router;