const { books } = require('../models')

class BookController {
    static getBook(req, res) {
        books.findAll()
            .then(result => {
                res.render('index.ejs', { books: result })
            })
            .catch(err => {
                console.log(err);
            })
    }
    static addFormBook(req, res) {
        res.render('addBook.ejs');
    }
    static addBook(req, res) {
        const {
            title,
            author,
            released_date,
            pages,
            genre
        } = req.body;
        books.create({
                title,
                author,
                released_date,
                pages,
                genre
            })
            .then(() => {
                res.redirect('/')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static findById(req, res) {
        const id = req.params.id;
        books.findOne({
                where: { id }
            })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deleteBook(req, res) {
        const id = req.params.id;
        books.destroy({
                where: { id }
            })
            .then(() => {
                res.redirect('/')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static updateBook(req, res) {
        const id = req.params.id;
        const {
            title,
            author,
            released_date,
            pages,
            genre
        } = req.body;
        books.update({
                title,
                author,
                released_date,
                pages,
                genre
            }, {
                where: { id }
            })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }
}

module.exports = BookController;